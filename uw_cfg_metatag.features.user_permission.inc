<?php

/**
 * @file
 * uw_cfg_metatag.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_metatag_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer meta tags'.
  $permissions['administer meta tags'] = array(
    'name' => 'administer meta tags',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: abstract'.
  $permissions['edit meta tag: abstract'] = array(
    'name' => 'edit meta tag: abstract',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: article:author'.
  $permissions['edit meta tag: article:author'] = array(
    'name' => 'edit meta tag: article:author',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: article:expiration_time'.
  $permissions['edit meta tag: article:expiration_time'] = array(
    'name' => 'edit meta tag: article:expiration_time',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: article:modified_time'.
  $permissions['edit meta tag: article:modified_time'] = array(
    'name' => 'edit meta tag: article:modified_time',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: article:published_time'.
  $permissions['edit meta tag: article:published_time'] = array(
    'name' => 'edit meta tag: article:published_time',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: article:publisher'.
  $permissions['edit meta tag: article:publisher'] = array(
    'name' => 'edit meta tag: article:publisher',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: article:section'.
  $permissions['edit meta tag: article:section'] = array(
    'name' => 'edit meta tag: article:section',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: article:tag'.
  $permissions['edit meta tag: article:tag'] = array(
    'name' => 'edit meta tag: article:tag',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: author'.
  $permissions['edit meta tag: author'] = array(
    'name' => 'edit meta tag: author',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: book:author'.
  $permissions['edit meta tag: book:author'] = array(
    'name' => 'edit meta tag: book:author',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: book:isbn'.
  $permissions['edit meta tag: book:isbn'] = array(
    'name' => 'edit meta tag: book:isbn',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: book:release_date'.
  $permissions['edit meta tag: book:release_date'] = array(
    'name' => 'edit meta tag: book:release_date',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: book:tag'.
  $permissions['edit meta tag: book:tag'] = array(
    'name' => 'edit meta tag: book:tag',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: cache-control'.
  $permissions['edit meta tag: cache-control'] = array(
    'name' => 'edit meta tag: cache-control',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: canonical'.
  $permissions['edit meta tag: canonical'] = array(
    'name' => 'edit meta tag: canonical',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: content-language'.
  $permissions['edit meta tag: content-language'] = array(
    'name' => 'edit meta tag: content-language',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: description'.
  $permissions['edit meta tag: description'] = array(
    'name' => 'edit meta tag: description',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: expires'.
  $permissions['edit meta tag: expires'] = array(
    'name' => 'edit meta tag: expires',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: generator'.
  $permissions['edit meta tag: generator'] = array(
    'name' => 'edit meta tag: generator',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: geo.placename'.
  $permissions['edit meta tag: geo.placename'] = array(
    'name' => 'edit meta tag: geo.placename',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: geo.position'.
  $permissions['edit meta tag: geo.position'] = array(
    'name' => 'edit meta tag: geo.position',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: geo.region'.
  $permissions['edit meta tag: geo.region'] = array(
    'name' => 'edit meta tag: geo.region',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: icbm'.
  $permissions['edit meta tag: icbm'] = array(
    'name' => 'edit meta tag: icbm',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: image_src'.
  $permissions['edit meta tag: image_src'] = array(
    'name' => 'edit meta tag: image_src',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: itemtype'.
  $permissions['edit meta tag: itemtype'] = array(
    'name' => 'edit meta tag: itemtype',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: keywords'.
  $permissions['edit meta tag: keywords'] = array(
    'name' => 'edit meta tag: keywords',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: news_keywords'.
  $permissions['edit meta tag: news_keywords'] = array(
    'name' => 'edit meta tag: news_keywords',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: next'.
  $permissions['edit meta tag: next'] = array(
    'name' => 'edit meta tag: next',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:audio'.
  $permissions['edit meta tag: og:audio'] = array(
    'name' => 'edit meta tag: og:audio',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:audio:secure_url'.
  $permissions['edit meta tag: og:audio:secure_url'] = array(
    'name' => 'edit meta tag: og:audio:secure_url',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:audio:type'.
  $permissions['edit meta tag: og:audio:type'] = array(
    'name' => 'edit meta tag: og:audio:type',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:country_name'.
  $permissions['edit meta tag: og:country_name'] = array(
    'name' => 'edit meta tag: og:country_name',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:description'.
  $permissions['edit meta tag: og:description'] = array(
    'name' => 'edit meta tag: og:description',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:determiner'.
  $permissions['edit meta tag: og:determiner'] = array(
    'name' => 'edit meta tag: og:determiner',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:email'.
  $permissions['edit meta tag: og:email'] = array(
    'name' => 'edit meta tag: og:email',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:fax_number'.
  $permissions['edit meta tag: og:fax_number'] = array(
    'name' => 'edit meta tag: og:fax_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:image'.
  $permissions['edit meta tag: og:image'] = array(
    'name' => 'edit meta tag: og:image',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:image:height'.
  $permissions['edit meta tag: og:image:height'] = array(
    'name' => 'edit meta tag: og:image:height',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:image:secure_url'.
  $permissions['edit meta tag: og:image:secure_url'] = array(
    'name' => 'edit meta tag: og:image:secure_url',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:image:type'.
  $permissions['edit meta tag: og:image:type'] = array(
    'name' => 'edit meta tag: og:image:type',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:image:url'.
  $permissions['edit meta tag: og:image:url'] = array(
    'name' => 'edit meta tag: og:image:url',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:image:width'.
  $permissions['edit meta tag: og:image:width'] = array(
    'name' => 'edit meta tag: og:image:width',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:latitude'.
  $permissions['edit meta tag: og:latitude'] = array(
    'name' => 'edit meta tag: og:latitude',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:locale'.
  $permissions['edit meta tag: og:locale'] = array(
    'name' => 'edit meta tag: og:locale',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:locale:alternate'.
  $permissions['edit meta tag: og:locale:alternate'] = array(
    'name' => 'edit meta tag: og:locale:alternate',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:locality'.
  $permissions['edit meta tag: og:locality'] = array(
    'name' => 'edit meta tag: og:locality',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:longitude'.
  $permissions['edit meta tag: og:longitude'] = array(
    'name' => 'edit meta tag: og:longitude',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:phone_number'.
  $permissions['edit meta tag: og:phone_number'] = array(
    'name' => 'edit meta tag: og:phone_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:postal_code'.
  $permissions['edit meta tag: og:postal_code'] = array(
    'name' => 'edit meta tag: og:postal_code',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:region'.
  $permissions['edit meta tag: og:region'] = array(
    'name' => 'edit meta tag: og:region',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:see_also'.
  $permissions['edit meta tag: og:see_also'] = array(
    'name' => 'edit meta tag: og:see_also',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:site_name'.
  $permissions['edit meta tag: og:site_name'] = array(
    'name' => 'edit meta tag: og:site_name',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:street_address'.
  $permissions['edit meta tag: og:street_address'] = array(
    'name' => 'edit meta tag: og:street_address',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:title'.
  $permissions['edit meta tag: og:title'] = array(
    'name' => 'edit meta tag: og:title',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:type'.
  $permissions['edit meta tag: og:type'] = array(
    'name' => 'edit meta tag: og:type',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:updated_time'.
  $permissions['edit meta tag: og:updated_time'] = array(
    'name' => 'edit meta tag: og:updated_time',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:url'.
  $permissions['edit meta tag: og:url'] = array(
    'name' => 'edit meta tag: og:url',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:video:height'.
  $permissions['edit meta tag: og:video:height'] = array(
    'name' => 'edit meta tag: og:video:height',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:video:secure_url'.
  $permissions['edit meta tag: og:video:secure_url'] = array(
    'name' => 'edit meta tag: og:video:secure_url',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:video:type'.
  $permissions['edit meta tag: og:video:type'] = array(
    'name' => 'edit meta tag: og:video:type',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:video:url'.
  $permissions['edit meta tag: og:video:url'] = array(
    'name' => 'edit meta tag: og:video:url',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: og:video:width'.
  $permissions['edit meta tag: og:video:width'] = array(
    'name' => 'edit meta tag: og:video:width',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: original-source'.
  $permissions['edit meta tag: original-source'] = array(
    'name' => 'edit meta tag: original-source',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: pragma'.
  $permissions['edit meta tag: pragma'] = array(
    'name' => 'edit meta tag: pragma',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: prev'.
  $permissions['edit meta tag: prev'] = array(
    'name' => 'edit meta tag: prev',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: profile:first_name'.
  $permissions['edit meta tag: profile:first_name'] = array(
    'name' => 'edit meta tag: profile:first_name',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: profile:gender'.
  $permissions['edit meta tag: profile:gender'] = array(
    'name' => 'edit meta tag: profile:gender',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: profile:last_name'.
  $permissions['edit meta tag: profile:last_name'] = array(
    'name' => 'edit meta tag: profile:last_name',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: profile:username'.
  $permissions['edit meta tag: profile:username'] = array(
    'name' => 'edit meta tag: profile:username',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: publisher'.
  $permissions['edit meta tag: publisher'] = array(
    'name' => 'edit meta tag: publisher',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: rating'.
  $permissions['edit meta tag: rating'] = array(
    'name' => 'edit meta tag: rating',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: referrer'.
  $permissions['edit meta tag: referrer'] = array(
    'name' => 'edit meta tag: referrer',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: refresh'.
  $permissions['edit meta tag: refresh'] = array(
    'name' => 'edit meta tag: refresh',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: revisit-after'.
  $permissions['edit meta tag: revisit-after'] = array(
    'name' => 'edit meta tag: revisit-after',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: rights'.
  $permissions['edit meta tag: rights'] = array(
    'name' => 'edit meta tag: rights',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: robots'.
  $permissions['edit meta tag: robots'] = array(
    'name' => 'edit meta tag: robots',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: shortlink'.
  $permissions['edit meta tag: shortlink'] = array(
    'name' => 'edit meta tag: shortlink',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: standout'.
  $permissions['edit meta tag: standout'] = array(
    'name' => 'edit meta tag: standout',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: title'.
  $permissions['edit meta tag: title'] = array(
    'name' => 'edit meta tag: title',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:app:country'.
  $permissions['edit meta tag: twitter:app:country'] = array(
    'name' => 'edit meta tag: twitter:app:country',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:app:id:googleplay'.
  $permissions['edit meta tag: twitter:app:id:googleplay'] = array(
    'name' => 'edit meta tag: twitter:app:id:googleplay',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:app:id:ipad'.
  $permissions['edit meta tag: twitter:app:id:ipad'] = array(
    'name' => 'edit meta tag: twitter:app:id:ipad',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:app:id:iphone'.
  $permissions['edit meta tag: twitter:app:id:iphone'] = array(
    'name' => 'edit meta tag: twitter:app:id:iphone',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:app:name:googleplay'.
  $permissions['edit meta tag: twitter:app:name:googleplay'] = array(
    'name' => 'edit meta tag: twitter:app:name:googleplay',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:app:name:ipad'.
  $permissions['edit meta tag: twitter:app:name:ipad'] = array(
    'name' => 'edit meta tag: twitter:app:name:ipad',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:app:name:iphone'.
  $permissions['edit meta tag: twitter:app:name:iphone'] = array(
    'name' => 'edit meta tag: twitter:app:name:iphone',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:app:url:googleplay'.
  $permissions['edit meta tag: twitter:app:url:googleplay'] = array(
    'name' => 'edit meta tag: twitter:app:url:googleplay',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:app:url:ipad'.
  $permissions['edit meta tag: twitter:app:url:ipad'] = array(
    'name' => 'edit meta tag: twitter:app:url:ipad',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:app:url:iphone'.
  $permissions['edit meta tag: twitter:app:url:iphone'] = array(
    'name' => 'edit meta tag: twitter:app:url:iphone',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:card'.
  $permissions['edit meta tag: twitter:card'] = array(
    'name' => 'edit meta tag: twitter:card',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:creator'.
  $permissions['edit meta tag: twitter:creator'] = array(
    'name' => 'edit meta tag: twitter:creator',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:creator:id'.
  $permissions['edit meta tag: twitter:creator:id'] = array(
    'name' => 'edit meta tag: twitter:creator:id',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:data1'.
  $permissions['edit meta tag: twitter:data1'] = array(
    'name' => 'edit meta tag: twitter:data1',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:data2'.
  $permissions['edit meta tag: twitter:data2'] = array(
    'name' => 'edit meta tag: twitter:data2',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:description'.
  $permissions['edit meta tag: twitter:description'] = array(
    'name' => 'edit meta tag: twitter:description',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:image'.
  $permissions['edit meta tag: twitter:image'] = array(
    'name' => 'edit meta tag: twitter:image',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:image0'.
  $permissions['edit meta tag: twitter:image0'] = array(
    'name' => 'edit meta tag: twitter:image0',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:image1'.
  $permissions['edit meta tag: twitter:image1'] = array(
    'name' => 'edit meta tag: twitter:image1',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:image2'.
  $permissions['edit meta tag: twitter:image2'] = array(
    'name' => 'edit meta tag: twitter:image2',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:image3'.
  $permissions['edit meta tag: twitter:image3'] = array(
    'name' => 'edit meta tag: twitter:image3',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:image:alt'.
  $permissions['edit meta tag: twitter:image:alt'] = array(
    'name' => 'edit meta tag: twitter:image:alt',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:image:height'.
  $permissions['edit meta tag: twitter:image:height'] = array(
    'name' => 'edit meta tag: twitter:image:height',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:image:width'.
  $permissions['edit meta tag: twitter:image:width'] = array(
    'name' => 'edit meta tag: twitter:image:width',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:label1'.
  $permissions['edit meta tag: twitter:label1'] = array(
    'name' => 'edit meta tag: twitter:label1',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:label2'.
  $permissions['edit meta tag: twitter:label2'] = array(
    'name' => 'edit meta tag: twitter:label2',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:player'.
  $permissions['edit meta tag: twitter:player'] = array(
    'name' => 'edit meta tag: twitter:player',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:player:height'.
  $permissions['edit meta tag: twitter:player:height'] = array(
    'name' => 'edit meta tag: twitter:player:height',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:player:stream'.
  $permissions['edit meta tag: twitter:player:stream'] = array(
    'name' => 'edit meta tag: twitter:player:stream',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:player:stream:content_type'.
  $permissions['edit meta tag: twitter:player:stream:content_type'] = array(
    'name' => 'edit meta tag: twitter:player:stream:content_type',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:player:width'.
  $permissions['edit meta tag: twitter:player:width'] = array(
    'name' => 'edit meta tag: twitter:player:width',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:site'.
  $permissions['edit meta tag: twitter:site'] = array(
    'name' => 'edit meta tag: twitter:site',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:site:id'.
  $permissions['edit meta tag: twitter:site:id'] = array(
    'name' => 'edit meta tag: twitter:site:id',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:title'.
  $permissions['edit meta tag: twitter:title'] = array(
    'name' => 'edit meta tag: twitter:title',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: twitter:url'.
  $permissions['edit meta tag: twitter:url'] = array(
    'name' => 'edit meta tag: twitter:url',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: video:actor'.
  $permissions['edit meta tag: video:actor'] = array(
    'name' => 'edit meta tag: video:actor',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: video:actor:role'.
  $permissions['edit meta tag: video:actor:role'] = array(
    'name' => 'edit meta tag: video:actor:role',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: video:director'.
  $permissions['edit meta tag: video:director'] = array(
    'name' => 'edit meta tag: video:director',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: video:duration'.
  $permissions['edit meta tag: video:duration'] = array(
    'name' => 'edit meta tag: video:duration',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: video:release_date'.
  $permissions['edit meta tag: video:release_date'] = array(
    'name' => 'edit meta tag: video:release_date',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: video:series'.
  $permissions['edit meta tag: video:series'] = array(
    'name' => 'edit meta tag: video:series',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: video:tag'.
  $permissions['edit meta tag: video:tag'] = array(
    'name' => 'edit meta tag: video:tag',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tag: video:writer'.
  $permissions['edit meta tag: video:writer'] = array(
    'name' => 'edit meta tag: video:writer',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit meta tags'.
  $permissions['edit meta tags'] = array(
    'name' => 'edit meta tags',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'metatag',
  );

  return $permissions;
}
